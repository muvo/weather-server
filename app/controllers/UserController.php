<?php

namespace app\controllers;

use app\models\User;
use yii\base\DynamicModel;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

class UserController extends Controller
{
    public function actionSignIn()
    {
        if (!\Yii::$app->user->getIsGuest()) {
            return $this->redirect(Url::home());
        }

        $model = (new DynamicModel(['login', 'password']))
            ->addRule('login', 'required')
            ->addRule(['login', 'password'], 'string');

        if (\Yii::$app->request->getIsPost()) {
            $model->load(\Yii::$app->request->post());
            if (!$model->validate()) {
                throw new BadRequestHttpException(current($model->firstErrors));
            } elseif ($user = User::auth($model->login, $model->password)) {
                \Yii::$app->user->login($user);
                return $this->redirect(\Yii::$app->user->getReturnUrl(Url::home()));
            }

            \Yii::$app->session->setFlash('login.warning', \Yii::t('app', 'Username or password incorrect'));
        }

        return $this->render('sign-in', compact('model'));
    }

    public function actionSignOut()
    {
        if (!\Yii::$app->user->getIsGuest()) {
            \Yii::$app->user->logout();
        }

        return $this->redirect(Url::home());
    }
}
