<?php
/**
 * @var \yii\web\View $this
 * @var \yii\base\DynamicModel $model
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = Yii::t('app', 'Authentication');

?>

<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><?= $this->title ?></h3>
            </div>
            <?php $form = ActiveForm::begin() ?>
            <div class="panel-body">
                <?php if (Yii::$app->session->hasFlash('login.warning')): ?>
                    <div class=""><?= Yii::$app->session->getFlash('login.warning') ?></div>
                <?php endif; ?>
                <?= $form->field($model, 'login')
                    ->input('email', ['placeholder' => Yii::t('app', 'Email')])
                    ->label(Yii::t('app', "User's login")) ?>

                <?= $form->field($model, 'password')
                    ->passwordInput(['placeholder' => Yii::t('app', 'Password')])
                    ->label(Yii::t('app', 'Password')) ?>
            </div>
            <div class="panel-footer text-right">
                <?= Html::submitButton(Yii::t('app', 'Sign in'),
                    ['class' => ['btn', 'btn-primary', 'btn-lg']]) ?>
            </div>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>
