<?php

namespace app\commands;

use app\components\openweathermap\Client;
use app\models\weather\City;
use app\models\weather\Data;
use Carbon\Carbon;
use yii\base\Event;
use yii\base\InvalidConfigException;
use yii\console\Controller;
use yii\db\AfterSaveEvent;
use yii\db\Connection;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;

class CityController extends Controller
{
    /**
     * @var Client $api
     */
    public $api;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->api = \Yii::$app->get('openweathermap');

        Event::on(Data::class, Data::EVENT_AFTER_INSERT,
            function (AfterSaveEvent $event) {
                /**
                 * @var Data $update
                 */
                $update = $event->sender;
                $update->city->setAttribute('last_poll', time());
                if (!$update->city->save()) {
                    \Yii::warning(\Yii::t('app', current($update->city->firstErrors)));
                }
            });
    }

    public function actionAdd($city, int $interval = 3600)
    {
        $weather = $this->api->weather(['q' => $city]);
        $rc = \Yii::$app->db->transaction(function (Connection $db) use ($interval, $weather) {
            $city = new City([
                'id' => $weather['id'],
                'name' => $weather['name'],
                'poll_interval' => $interval,
            ]);
            if (!$city->save()) {
                throw new InvalidConfigException(current($city->firstErrors));
            }

            $weather = new Data([
                'city_id' => $weather['id'],
                'data' => $weather,
            ]);
            if (!$weather->save()) {
                throw new InvalidConfigException(current($weather->firstErrors));
            }

            return true;
        });

        return $rc === true ? 0 : 1;
    }

    public function actionPoll($city = null)
    {
        $cities = City::find()
            ->where(['is_active' => 1])
            ->andWhere(['<=', new Expression('last_poll+poll_interval'), time()])
            ->andFilterWhere(['or',
                ['id' => $city],
                ['name' => $city],
            ]);

        foreach ($cities->batch(20) as $chunk) {
            $cities_ids = ArrayHelper::getColumn($chunk, 'id');
            $result = $this->api->group(['id' => implode(',', $cities_ids)]);
            foreach ($result['list'] as $w) {
                $data = new Data([
                    'city_id' => $w['id'],
                    'data' => $w,
                ]);
                if (!$data->save()) {
                    $this->stderr(\Yii::t('app', "Can't save weather info for «{0}»", $w['name']), Console::FG_YELLOW);
                }
            }
        }
    }
}
