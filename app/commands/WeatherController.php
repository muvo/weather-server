<?php

namespace app\commands;

use app\components\openweathermap\Client;
use app\models\weather\Data;
use yii\console\Controller;
use yii\helpers\Console;
use yii\helpers\VarDumper;

class WeatherController extends Controller
{
    /**
     * @var Client $api
     */
    public $api;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->api = \Yii::$app->get('openweathermap');
    }

    public function actionIndex(string $city)
    {
        $weather = $this->api->weather(['q' => $city]);

        $this->stdout(VarDumper::dumpAsString($weather));

        if (!($data = new Data(['data' => $weather, 'city_id' => $weather['id']]))->save()) {
            $this->stderr(current($data->firstErrors), Console::BG_RED);
        }
    }
}
