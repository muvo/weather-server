<?php

namespace app\components\openweathermap;

use GuzzleHttp\RequestOptions;
use yii\base\Component;
use yii\helpers\Json;

/**
 * Class Client
 * @package app\components\openweathermap
 *
 * @method weather(array $options)
 * @method group(array $options)
 */
class Client extends Component
{
    /**
     * @var string|null
     */
    public $apiKey;

    /**
     * @var \GuzzleHttp\Client
     */
    private $httpClient;

    public function init()
    {
        $this->httpClient = new \GuzzleHttp\Client([
            'base_uri' => 'https://api.openweathermap.org/data/2.5/',
            'verify' => false,
//            'exceptions' => false,
        ]);
    }

    /**
     * @param $name
     * @param array $options
     * @return string
     */
    public function call($name, array $options = [])
    {
        $response = $this->httpClient->get($name,
            [
                RequestOptions::QUERY => ['APPID' => $this->apiKey]
                    + $options + ['units' => 'metric'],
            ])
            ->getBody()
            ->getContents();
        \Yii::debug($response, __METHOD__);

        return Json::decode($response);
    }

    /**
     * @inheritdoc
     */
    public function __call($name, $params)
    {
        if (method_exists($this, $name)) {
            return parent::__call($name, $params);
        }

        return $this->call($name, $params[0] ?? []);
    }
}
