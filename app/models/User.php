<?php

namespace app\models;

use Ramsey\Uuid\Uuid;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class User extends ActiveRecord implements IdentityInterface
{
    /**
     * @var string|null $_password
     */
    private $_password;

    /**
     * @var string|null $password_retype
     */
    public $password_retype;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%users}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['id', 'default',
                'value' => Uuid::uuid1()->toString(),
            ],
            ['login', 'required'],
            ['login', 'email'],
            ['login', 'unique'],
            ['password_retype', 'compare',
                'compareAttribute' => 'password',
                'skipOnEmpty' => false,
                'when' => function () {
                    return !empty($this->password);
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        return [
            'id',
            'login',
            'created_at',
            'updated_at',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getAttribute('id');
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @param string $login
     * @param string $password
     * @return User|null
     */
    public static function auth(string $login, string $password = '')
    {
        if (!$user = static::findOne(['login' => $login])) {
            \Yii::warning(sprintf('User «%s» not found', $login), __METHOD__);
            return null;
        } elseif (!empty($user->pwhash) && !\Yii::$app->security->validatePassword($password, $user->pwhash)) {
            \Yii::warning(sprintf('Password mismatch for «%s»', $login), __METHOD__);
            return null;
        }

        return $user;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        if (\Yii::$app->has('cache')) {
            return \Yii::$app->cache->exists($token)
                ? static::findIdentity(\Yii::$app->cache->get($token))
                : null;
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return \Yii::$app->security
            ->hashData($this->id, $this->pwhash);
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return \Yii::$app->security
            ->compareString($this->getAuthKey(), $authKey);
    }

    public function setPassword(string $password)
    {
        $this->setAttribute('pwhash', \Yii::$app->security->generatePasswordHash($this->_password = $password));
        return $this;
    }

    public function getPassword()
    {
        return $this->_password;
    }
}
