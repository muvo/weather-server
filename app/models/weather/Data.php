<?php

namespace app\models\weather;

use Ramsey\Uuid\Uuid;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;

/**
 * Class Data
 * @package app\models\weather
 *
 * @property City|null $city
 */
class Data extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%weather_data}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::class,
                'attributes' => [static::EVENT_BEFORE_INSERT => 'id'],
                'preserveNonEmptyValues' => true,
                'value' => Uuid::uuid1()->toString(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['data', 'required'],
            ['created_at', 'default',
                'value' => time(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }
}
