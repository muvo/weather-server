<?php

namespace app\models\weather;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Class City
 * @package app\models\weather
 *
 * @property Data[]|array $data
 * @property-read Data|null $lastUpdate
 */
class City extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cities}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [TimestampBehavior::class];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['is_active', 'boolean'],
            ['poll_interval', 'integer',
                'min' => 300,
                'when' => function () {
                    return $this->isAttributeChanged('poll_interval', false);
                },
            ],
            ['last_poll', 'integer',
                'min' => time() - 1,
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getData()
    {
        return $this->hasMany(Data::class, ['city_id' => 'id'])
            ->inverseOf('city');
    }

    /**
     * @return Data|null
     */
    public function getLastUpdate()
    {
        return $this->getData()
            ->orderBy(['created_at' => SORT_DESC])
            ->one();
    }
}
