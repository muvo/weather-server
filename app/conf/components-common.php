<?php return [
    'cache' => \yii\caching\FileCache::class,
    'db' => require(__DIR__ . '/com/db.php'),
    'openweathermap' => [
        'class' => \app\components\openweathermap\Client::class,
        'apiKey' => getenv('OPENWEATHERMAP_API_KEY') ?: null,
    ],
];
