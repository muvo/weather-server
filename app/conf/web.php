<?php return [
    'id' => '__APPLICATION__',
    'name' => getenv('APP_NAME'),
    'basePath' => dirname(__DIR__),
    'vendorPath' => dirname(__DIR__) . '/../vendor',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'bootstrap' => ['log'],
    'layout' => 'bootstrap',
    'defaultRoute' => '/admin/user',
    'components' => \yii\helpers\ArrayHelper::merge(require(__DIR__ . '/components-common.php'),
        [
            'log' => [
                'targets' => [\yii\log\FileTarget::class],
            ],
            'urlManager' => [
                'enablePrettyUrl' => true,
                'showScriptName' => getenv('APP_ENVIRONMENT') !== 'prod',
            ],
            'request' => [
                'cookieValidationKey' => getenv('APP_KEY'),
            ],
            'user' => [
                'class' => \yii\web\User::class,
                'identityClass' => \app\models\User::class,
                'loginUrl' => ['/user/sign-in'],
                'enableAutoLogin' => true,
                'enableSession' => true,
            ],
        ]),
    'modules' => [
        'admin' => \app\modules\admin\Module::class,
        'api' => \app\modules\api\Module::class,
    ],
    'params' => require(__DIR__ . '/params.php'),
];
