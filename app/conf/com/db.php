<?php return function () {
    $database = [
        'class' => \yii\db\Connection::class,
        'username' => getenv('DATABASE_USER') ?: 'root',
        'password' => getenv('DATABASE_PASS') ?: null,
    ];

    if (!$database['dsn'] = getenv('DATABASE_DSN')) {
//        TODO: implement building DSN-string via arbitrary ENV-variables, like DB_DRIVER, DB_HOST, DB_PORT, etc…
    }

    return Yii::createObject($database);
};
