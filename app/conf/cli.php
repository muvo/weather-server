<?php return [
    'id' => '__APPLICATION__ CLI',
    'name' => getenv('APP_NAME'),
    'basePath' => dirname(__DIR__),
    'vendorPath' => dirname(__DIR__) . '/../vendor',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'components' => \yii\helpers\ArrayHelper::merge(require(__DIR__ . '/components-common.php'),
        [
            'log' => [
                'targets' => [
                    [
                        'class' => \yii\log\FileTarget::class,
                        'logFile' => '@runtime/logs/cli.log',
                    ],
                ],
            ],
        ]),
    'params' => require(__DIR__ . '/params.php'),
];
