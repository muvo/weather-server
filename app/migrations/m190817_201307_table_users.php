<?php

use yii\db\Migration;

/**
 * Class m190817_201307_table_users
 */
class m190817_201307_table_users extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(\app\models\User::tableName(),
            [
                'id' => $this->string(36)->notNull(),
                'login' => $this->string()->notNull(),
                'pwhash' => $this->string()->null(),
                'created_at' => $this->integer()->notNull(),
                'updated_at' => $this->integer()->null(),
            ]);
        $this->addPrimaryKey('PK_id', \app\models\User::tableName(), 'id');
        $this->createIndex('UK_login', \app\models\User::tableName(), 'login', true);

        (new \app\models\User(['login' => 'root@app.local']))
            ->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(\app\models\User::tableName());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190817_201307_table_users cannot be reverted.\n";

        return false;
    }
    */
}
