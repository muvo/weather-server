<?php

use yii\db\Migration;

/**
 * Class m190821_213128_table_cities
 */
class m190821_213128_table_cities extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(\app\models\weather\City::tableName(),
            [
                'id' => $this->integer()->notNull(),
                'name' => $this->string()->notNull(),
                'is_active' => $this->boolean()->defaultValue(1),
                'poll_interval' => $this->integer()->notNull()->defaultValue(3600),
                'last_poll' => $this->integer()->null(),
                'created_at' => $this->integer()->notNull(),
                'updated_at' => $this->integer(),
            ]);
        $this->addPrimaryKey('PK_id', \app\models\weather\City::tableName(), 'id');
        $this->createIndex('IDX_active', \app\models\weather\City::tableName(), 'is_active');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(\app\models\weather\City::tableName());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190821_213128_table_cities cannot be reverted.\n";

        return false;
    }
    */
}
