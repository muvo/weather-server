<?php

use yii\db\Migration;

/**
 * Class m190817_201431_table_weather_data
 */
class m190817_201431_table_weather_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(\app\models\weather\Data::tableName(),
            [
                'id' => $this->string()->notNull(),
                'created_at' => $this->integer()->notNull(),
                'city_id' => $this->integer()->notNull(),
                'data' => $this->json()->notNull(),
            ]);
        $this->addPrimaryKey('PK_id', \app\models\weather\Data::tableName(), 'id');
        $this->createIndex('IDX_city', \app\models\weather\Data::tableName(), 'city_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(\app\models\weather\Data::tableName());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190817_201431_table_weather_data cannot be reverted.\n";

        return false;
    }
    */
}
