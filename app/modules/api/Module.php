<?php

namespace app\modules\api;

use yii\filters\auth\HttpHeaderAuth;

class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->setBasePath(__DIR__);

        \Yii::$app->user->enableSession = false;
        \Yii::$app->user->enableAutoLogin = false;

        $this->attachBehavior('access',
            [
                'class' => HttpHeaderAuth::class,
                'except' => ['user/auth'],
            ]);
    }
}
