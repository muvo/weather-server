<?php

namespace app\modules\api\controllers;

use app\models\weather\City;
use yii\helpers\ArrayHelper;
use yii\rest\Controller;

class WeatherController extends Controller
{
    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'index' => ['GET'],
        ];
    }

    public function actionIndex()
    {
        $cities = City::find()
            ->where(['is_active' => 1])
            ->orderBy(['last_poll' => SORT_DESC]);

        return ArrayHelper::map($cities->all(), 'name', 'lastUpdate.data.main.temp');
    }
}
