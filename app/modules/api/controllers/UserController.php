<?php

namespace app\modules\api\controllers;

use app\models\User;
use Ramsey\Uuid\Uuid;
use yii\web\ForbiddenHttpException;
use yii\web\ServerErrorHttpException;

final class UserController extends \yii\rest\Controller
{
    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'auth' => ['POST'],
            'who-am-i' => ['GET'],
        ];
    }

    public function actionAuth()
    {
        if (!$user = User::auth(\Yii::$app->request->post('login', ''), \Yii::$app->request->post('password', ''))) {
            throw new ForbiddenHttpException(\Yii::t('app', 'Access denied'));
        }

        if (!\Yii::$app->cache->set($token = Uuid::uuid4()->toString(), $user->getId(), 3600)) {
            throw new ServerErrorHttpException(\Yii::t('app', "Can't auth user"));
        }

        return compact('token');
    }

    public function actionWhoAmI()
    {
        return \Yii::$app->user->identity;
    }
}
