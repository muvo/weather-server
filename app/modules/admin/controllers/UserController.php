<?php

namespace app\modules\admin\controllers;

use app\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

class UserController extends Controller
{
    public function actionIndex()
    {
        $users = User::find()
            ->where(['!=', 'id', \Yii::$app->user->identity->getId()]);

        return $this->render('index', compact('users'));
    }

    public function actionEdit($user_id = null)
    {
        $user = is_null($user_id)
            ? \Yii::createObject(User::class)
            : User::findOne($user_id);

        if (\Yii::$app->request->getIsPost()) {
            $user->load(\Yii::$app->request->post());
            if (!$user->save()) {
                \Yii::$app->session->setFlash('edit.warning', \Yii::t('app', current($user->firstErrors)));
            } elseif (!empty($user_id)) {
                \Yii::$app->session->setFlash('edit.success', \Yii::t('app', 'User saved'));
                return $this->refresh();
            }

            return $this->redirect(['user/index']);
        }

        return $this->render('edit', compact('user'));
    }

    public function actionDelete($user_id)
    {
        if (!$user = User::findOne($user_id)) {
            throw new NotFoundHttpException(\Yii::t('app', 'User not found'));
        } elseif (!$user->delete()) {
            throw new ServerErrorHttpException("Can't delete user");
        }

        return $this->redirect(\Yii::$app->request->getReferrer());
    }
}
