<?php
/**
 * @var \yii\web\View $this
 * @var \app\models\User $user
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = Yii::t('app', 'User management');
?>

<h1><?= Html::encode($this->title) ?></h1>
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="panel panel-default">
            <?php $form = ActiveForm::begin() ?>
            <div class="panel-body">
                <?php if (Yii::$app->session->hasFlash('edit.warning')): ?>
                    <div class="alert alert-danger">
                        <?= Yii::$app->session->getFlash('edit.warning') ?>
                    </div>
                <?php elseif (Yii::$app->session->hasFlash('edit.success')): ?>
                    <div class="alert alert-success">
                        <?= Yii::$app->session->getFlash('edit.success') ?>
                    </div>
                <?php endif; ?>

                <?= $form->field($user, 'login')
                    ->input('email', ['placeholder' => 'Email']) ?>

                <?= $form->field($user, 'password')
                    ->passwordInput() ?>

                <?= $form->field($user, 'password_retype')
                    ->passwordInput() ?>
            </div>
            <div class="panel-footer">
                <?= Html::submitButton(Yii::t('app', 'Apply'),
                    ['class' => ['btn', 'btn-primary', 'btn-lg']]) ?>
            </div>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>