<?php
/**
 * @var \yii\web\View $this
 * @var \yii\db\ActiveQuery $users
 */

use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = Yii::t('app', 'Users list');

?>
<h1 class="page-header"><?= Html::encode($this->title) ?></h1>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <?= GridView::widget([
            'dataProvider' => new ActiveDataProvider([
                'query' => $users,
            ]),
            'columns' => [
                [
                    'class' => \yii\grid\SerialColumn::class,
                ],
                [
                    'attribute' => 'login',
                    'content' => function (\app\models\User $user) {
                        return Html::a($user->login, ['user/edit', 'user_id' => $user->id]);
                    },
                ],
                'created_at:datetime',
                'updated_at:datetime',
                [
                    'content' => function (\app\models\User $user) {
                        return Html::a('Delete', ['user/delete', 'user_id' => $user->id],
                            [
                                'class' => ['btn', 'btn-xs', 'btn-danger'],
                                'onclick' => new \yii\web\JsExpression('return window.confirm("' . Yii::t('app', 'Confirm operation!') . '")'),
                            ]);
                    }
                ],
            ],
        ]) ?>
        <?= Html::a(Yii::t('app', 'Create new user'), ['user/edit'],
            ['class' => ['btn', 'btn-primary']]) ?>
    </div>
</div>
