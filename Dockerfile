FROM muvo/php7-fpm-composer:current

COPY --chown=33:33 . /application

WORKDIR /application

USER 33

RUN composer install

VOLUME [ "/application/runtime", "/application/public/assets" ]

EXPOSE 8000/tcp
